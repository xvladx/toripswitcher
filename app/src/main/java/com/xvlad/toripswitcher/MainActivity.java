package com.xvlad.toripswitcher;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    static TorController controller;
    static SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        MainActivity.pref = PreferenceManager.getDefaultSharedPreferences(this);
        Integer port = Integer.valueOf(MainActivity.pref.getString(getResources().getString(R.string.CONST_tor_control_port),
                String.valueOf(getResources().getInteger(R.integer.tor_control_port))));
        String password = MainActivity.pref.getString(getResources().getString(R.string.CONST_tor_control_password),
                getResources().getString(R.string.tor_control_password));

        String excludedIps = MainActivity.pref.getString(getResources().getString(R.string.CONST_tor_excluded_ips),
                "");

        MainActivity.controller = new TorController(port, password);
        try {
            setExcludedIps(excludedIps);
        } catch (IOException exception) {
            Log.e("Excluded IPs", exception.getMessage());
        }

        setContentView(R.layout.activity_main);
    }

    public void changeIpClick(View view) {
        EditText currentIp = (EditText) findViewById(R.id.currentIp);
        try {
            String newIp = torChange();
            currentIp.setText(newIp);
        } catch (IOException exception) {
            currentIp.setText(exception.getMessage());
        }
    }

    public void getIpClick(View view) {
        EditText currentIp = (EditText) findViewById(R.id.currentIp);
        String text = "Can't get IP";
        try {
            text = MainActivity.controller.getCurrentIp();
        } catch (IOException exception) {
            System.out.println("Met exception: "+exception.getMessage());
        } finally {
            currentIp.setText(text);
        }
    }

    public void settingsClick(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void setExcludedIps(String excludedIps) throws IOException {
        //Сброс исплюченных
        MainActivity.controller.setConfig(new String[] {"ExcludeNodes", ""}, false);

        MainActivity.controller.setConfig(new String[] {"ExcludeNodes", excludedIps}, false);

        if(excludedIps.length() > 0) {
            MainActivity.controller.sendSignal(TorController.SIGNAL_NEW_IDENTITY);
        }
    }

    private String torChange() throws IOException {
        //getting ip
        String currentIp = MainActivity.controller.getCurrentIp();

        StringBuilder excludedIps = new StringBuilder();
        String[] Config = MainActivity.controller.getConfig(new String[] {"ExcludeNodes"});
        if(Config[0].length() > 0) {
            excludedIps.append(Config[0]).append(",").append(currentIp);
        } else {
            excludedIps.append(currentIp);
        }

        //Сброс исплюченных
        MainActivity.controller.setConfig(new String[] {"ExcludeNodes", ""}, false);

        MainActivity.controller.setConfig(new String[] {"ExcludeNodes", excludedIps.toString()}, false);
        MainActivity.controller.sendSignal(TorController.SIGNAL_NEW_IDENTITY);

        MainActivity.pref.edit().putString(getResources().getString(R.string.CONST_tor_excluded_ips), excludedIps.toString()).apply();
        return MainActivity.controller.getCurrentIp();
    }
}
