/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xvlad.toripswitcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.freeheaven.tor.control.*;

/**
 * @author vlad
 */
public class TorController implements TorControlCommands {

    private Integer port;
    private String password;
    private TorControlConnection connection;

    public TorController(Integer port, String password) {
        this.port = port;
        this.password = password;
    }

    private TorControlConnection getConnection()
            throws IOException {
        if (this.connection != null) {
            return this.connection;
        } else {
            TorControlConnection conn = TorControlConnection.getConnection(
                    new java.net.Socket("127.0.0.1", this.port));
            conn.launchThread(true);
            conn.authenticate(this.password);
            return this.connection = conn;
        }
    }

    public Map<String, String> getInfo(String[] params) throws IOException {
        return this.getConnection().getInfo(Arrays.asList(params));
    }

    public void setConfig(String[] params, boolean save) throws IOException {
        // Usage: "set-config [-save] key value key value key value"
        TorControlConnection conn = this.getConnection();
        conn.setConf(Arrays.asList(params));
        if (save) {
            conn.saveConf();
        }
    }

    public String[] getConfig(String[] params) throws IOException {
        // Usage: get-config key key key
        TorControlConnection conn = this.getConnection();
        List<ConfigEntry> lst = conn.getConf(Arrays.asList(params));
        String[] returnVals = new String[lst.size()];

        int idx = 0;
        for (Iterator<ConfigEntry> i = lst.iterator(); i.hasNext(); ) {
            ConfigEntry e = i.next();
            returnVals[idx] = e.value;
            idx++;
        }
        return returnVals;
    }

    public String getCurrentIp() throws MalformedURLException, IOException {

        String url = "http://ifconfig.me/ip";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");

        InputStreamReader streamReader = new InputStreamReader(con.getInputStream(), "UTF-8");
        BufferedReader in = new BufferedReader(streamReader);
        String inputLine = "";
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        con.disconnect();

        return response.toString();
    }

    public void sendSignal(String signal) throws IOException {
        // Usage: get-config key key key
        TorControlConnection conn = this.getConnection();
        conn.signal(signal);
    }
}
